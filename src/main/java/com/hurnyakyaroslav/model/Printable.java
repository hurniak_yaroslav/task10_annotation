package com.hurnyakyaroslav.model;

@FunctionalInterface
public interface Printable {
    void print();
}
